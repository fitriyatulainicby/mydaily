from django.urls import path
from . import views

urlpatterns = [
    path('myweb/', views.myweb, name='myweb'), 
    path('mywebs/', views.mywebs, name='mywebs'),   
]
